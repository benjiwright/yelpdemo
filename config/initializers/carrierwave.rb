# CarrierWave.configure do |config|
#   config.fog_credentials = {
#     :provider               => 'AWS',
#     :aws_access_key_id      => ENV["aws_access_key_id"],
#     :aws_secret_access_key  => ENV["aws_secret_access_key"]
#   }
#   config.fog_directory  = ENV["fog_directory"]
# end



# Note: finding where the Google key pairs are found was not easy
# 'Interoperable Access' had to be found and turned on
# 1) https://console.developers.google.com
# 2) Storage -> Cloud Storage -> Interoperability 
# 3) create new key
CarrierWave.configure do |config|
  config.fog_credentials = {
    :provider                         => 'Google',
    :google_storage_access_key_id     => ENV["google_access_key"],
    :google_storage_secret_access_key => ENV["google_secrect"]
  }
  config.fog_directory = ENV["fog_directory"]
end

