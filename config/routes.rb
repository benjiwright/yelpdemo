Rails.application.routes.draw do
  
  devise_for :users
  
  
  resources :restaurants do 
    
    # add searchkick url  http://restaurants/search
    collection do
      get 'search' 
    end
    
    # nest Reviews for better db association, 
    # also provides http://restaurants/4/reviews/12 route
    resources :reviews, except: [:show, :index]
  end


  get 'pages/about'
  get 'pages/contact'
  
  root 'restaurants#index'

end
